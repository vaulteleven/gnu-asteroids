#include "GPlayer.hpp"
#include <math.h>

GPlayer::GPlayer()
{
    // player sprite initialization
    m_pPlayerSprite = new GSprite();
    m_pPlayerSprite->Load("assets/images/player_ship2.png");

    // set the player position at the mid of the screen
    m_fPlayerX = g_Framework->GetWindowWidth() / 2.0f;
    m_fPlayerY = g_Framework->GetWindowHeight() - m_pPlayerSprite->GetRect().h;

    // set the player position to the middle of the screen
    m_pPlayerSprite->SetPosition(m_fPlayerX, m_fPlayerY);

    m_pShotSprite = new GSprite();
    m_pShotSprite->Load("assets/images/shot.png");

    m_pShot = nullptr;
    lShots = new std::list<GShot*>;

    // velocity is measured in pixels / seconds
    m_fVelocity = 300.0f;
    m_fAcceleration = 0.0f;
    m_fSpeed = 0.0f;

    // angle of the player ship
    m_fAngle = 0.0f;
    m_fAngleSpeed = 240.0f;

    m_bShotLock = false;
    m_fShotTimer = 0.0f;
}

GPlayer::~GPlayer()
{
    SDL_DestroyTexture(m_pPlayerSprite->GetTexture());
    delete(m_pPlayerSprite);
    m_pPlayerSprite = nullptr;

    SDL_DestroyTexture(m_pShotSprite->GetTexture());
    delete(m_pShotSprite);
    m_pShotSprite = nullptr;
}

void GPlayer::Render()
{
    // handle shot rendering
    if(!lShots->empty())
    {
        for(lShotsIter=lShots->begin(); lShotsIter!=lShots->end(); lShotsIter++)
        {
            // * for dereferencing the Iterator and getting the element in 
            // the list it is pointing at;
            if((*lShotsIter)->GetIsAlive())
            {
                (*lShotsIter)->Update();
            }
            else
            {
                // delete the "dead" shot and free its memory;
                delete(*lShotsIter);
                (*lShotsIter) = nullptr;

                lShotsIter = lShots->erase(lShotsIter);
            }
        }
    }

    // set the new position from the update loop and draw the player sprite
    m_pPlayerSprite->SetPosition(m_fPlayerX, m_fPlayerY);
    m_pPlayerSprite->Render(m_fAngle);
}

void GPlayer::HandleMovement()
{
    // TODO implement "real" ship mass physics, so the ship behaves correctly 
    // whilst flying.
    //
    // handle keyboard input and move player accordingly 
    //
    // move ship forwards
    if(g_Framework->KeyDown(SDL_SCANCODE_W))
    {
        m_fAcceleration += 2.25f * g_Timer->GetTime();

        if(m_fAcceleration >= 1.0f)
            m_fAcceleration = 1.0f;

        m_fSpeed = m_fAcceleration * m_fVelocity * g_Timer->GetTime();

        m_fPlayerX += m_fSpeed * sin(m_fAngle*M_PI/180);
        m_fPlayerY -= m_fSpeed * cos(m_fAngle*M_PI/180);

        if(!CheckBoundsLeft())
            m_fPlayerX = g_Framework->GetWindowWidth() - 1;
        if(!CheckBoundsRight())
            m_fPlayerX = 0 - m_pPlayerSprite->GetRect().w;

        if(!CheckBoundsUpper())
            m_fPlayerY = g_Framework->GetWindowHeight() - 1;
        if(!CheckBoundsLower())
            m_fPlayerY = 0 - m_pPlayerSprite->GetRect().h;
    }

    /*
    // move ship backwards
    if(g_Framework->KeyDown(SDL_SCANCODE_S))
    {
        //m_fPlayerY += m_fVelocity * g_Timer->GetTime();
        m_fPlayerX -= m_fVelocity * g_Timer->GetTime() * sin(m_fAngle*M_PI/180);
        m_fPlayerY += m_fVelocity * g_Timer->GetTime() * cos(m_fAngle*M_PI/180);
    }
    */
    // rotate ship to the right
    if(g_Framework->KeyDown(SDL_SCANCODE_D))
    {
        m_fAngle += m_fAngleSpeed * g_Timer->GetTime();

        if(m_fAngle >= 360)
            m_fAngle = 0;
    }
    // rotate ship to the left
    if(g_Framework->KeyDown(SDL_SCANCODE_A))
    {
        m_fAngle -= m_fAngleSpeed * g_Timer->GetTime();

        if(m_fAngle <= -360)
            m_fAngle = 0;
    }

    if(!g_Framework->KeyDown(SDL_SCANCODE_W))
        GPlayer::HandleFlying();
}

void GPlayer::HandleShots()
{
    // if keyboard space is pressed and shotlock is not active
    if(g_Framework->KeyDown(SDL_SCANCODE_SPACE))
    {
        if(m_fShotTimer >= 0.15f)
        {
            // create new shot and set position according to player sprite
            // the - 1.0f is for the shots width
            //
            // the shot is rendered on a circle when the ship is rotating
            //
            // Xshot = Xorigin + r * cos(a) 
            // Yshot = Yorigin + r * sin(a) 
            //
            // r is half the height of the ship
            // a is m_fAngle of the ship -90.0f degrees!
            // X and Y origin is the origin => half the ships height
            //
            int Xshot = 
                (m_fPlayerX + m_pPlayerSprite->GetRect().w / 2.0f - 1.0f) + 
                (m_pPlayerSprite->GetRect().h - 2.0f) / 2.0f * 
                cos((m_fAngle-90.0f)*M_PI/180);

            int Yshot = 
                (m_fPlayerY + m_pPlayerSprite->GetRect().h / 2.0f) + 
                (m_pPlayerSprite->GetRect().h - 2.0f) / 2.0f * 
                sin((m_fAngle-90.0f)*M_PI/180);

            m_pShot = new GShot(m_pShotSprite, Xshot, Yshot, m_fAngle);

            lShots->push_back(m_pShot);

            m_fShotTimer = 0.0f;
        }
    }

    // shottimer for autoshot
    m_fShotTimer += g_Timer->GetTime();
}

bool GPlayer::CheckBoundsLeft()
{
    if(m_fPlayerX <= 0 - m_pPlayerSprite->GetRect().w)
        return false;

    return true;
}

bool GPlayer::CheckBoundsRight()
{
    if(m_fPlayerX >= g_Framework->GetWindowWidth())
        return false;

    return true;
}

bool GPlayer::CheckBoundsUpper()
{
    if(m_fPlayerY <= 0 - m_pPlayerSprite->GetRect().h)
        return false;

    return true;
}

bool GPlayer::CheckBoundsLower()
{
    if(m_fPlayerY >= g_Framework->GetWindowHeight())
        return false;

    return true;
}

void GPlayer::HandleFlying()
{
    m_fAcceleration -= 1.5f * g_Timer->GetTime();

    if(m_fAcceleration <= 0.0f)
        m_fAcceleration = 0.0f;

    m_fSpeed = m_fAcceleration * m_fVelocity * g_Timer->GetTime();

    if(m_fSpeed <= 0.0f)
        m_fSpeed = 0;

    m_fPlayerX += m_fSpeed * sin(m_fAngle*M_PI/180);
    m_fPlayerY -= m_fSpeed * cos(m_fAngle*M_PI/180);
}

void GPlayer::Update()
{
    GPlayer::HandleMovement();
    GPlayer::HandleShots();
    GPlayer::Render();
}
