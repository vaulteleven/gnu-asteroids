#ifndef GSPRITEHPP
#define GSPRITEHPP

#include "GFramework.hpp"

class GSprite
{
    public:
        GSprite();
        ~GSprite();

        bool Load(const std::string SpritePath);
        bool Load(const std::string SpritePath, bool fullscreen);
        void SetPosition(int SpriteX, int SpriteY);
        SDL_Texture *GetTexture() { return m_pTexture; }
        SDL_Rect GetRect() { return m_Rect; }

        bool GetIsAlive() { return m_bIsAlive; }
        bool SetIsAlive(bool value) { m_bIsAlive = value; }

        void Render();
        void Render(double angle);

    private:
        bool m_bFullscreen;

    protected:
        SDL_Texture *m_pTexture;
        SDL_Renderer *m_pRenderer;
        SDL_Rect m_Rect;
        bool m_bIsAlive;
};

#endif
