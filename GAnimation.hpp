#ifndef GANIMATIONHPP
#define GANIMATIONHPP

#include "GFramework.hpp"
#include "GSprite.hpp"

class GAnimation : public GSprite {

    public:
        GAnimation();
        ~GAnimation();
        // load animation sprite with w and h as widht and height of single 
        // animation frame, count is the number of frames
        bool LoadAnim(const std::string AnimPath, int w, int h, int count);
        bool LoadAnim(SDL_Texture *texture, int w, int h, int count);
        void Update();

    private:
        void RenderAnim();

        int m_AnimFrameCount;
        int m_AnimFrameWidth;
        int m_AnimFrameHeight;
        float m_fAnimTimer;
        int m_AnimPhase;
};

#endif
