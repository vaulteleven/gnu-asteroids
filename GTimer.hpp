#ifndef GTIMER_HPP
#define GTIMER_HPP

#include <SDL2/SDL.h>
#include "TSingleton.hpp"


#define g_Timer GTimer::Get()
class GTimer : public TSingleton<GTimer>
{
    public:
        GTimer();
        ~GTimer();

        float GetTime();
        void UpdateTime();

    private:
        float m_fCurrentTime;
        float m_fLastTime;
        float m_fTimePassed;
};

#endif
