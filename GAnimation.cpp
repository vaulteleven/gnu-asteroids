#include "GAnimation.hpp"

GAnimation::GAnimation()
{
    m_pRenderer = g_Framework->GetRenderer();
    m_pTexture = nullptr;
    m_bIsAlive = false;

    m_fAnimTimer = 0.0f;
    m_AnimPhase = 0;
}

GAnimation::~GAnimation()
{
}

// this function does the same as LoadAnim, but there is a Texture pointer 
// available, so one texture can be used for many animations.
bool GAnimation::LoadAnim(SDL_Texture *texture, int w, int h, int count)
{
    if(texture == nullptr)
        return false; 

    m_pTexture = texture;

    m_Rect.x = 0;
    m_Rect.y = 0;
    m_Rect.h = h;
    m_Rect.w = w;

    m_AnimFrameCount = count;
    m_AnimFrameHeight = h;
    m_AnimFrameWidth = w;

    return true;
}

bool GAnimation::LoadAnim(const std::string AnimPath, int w, int h, int count)
{
    SDL_Surface *m_pSprite = IMG_Load(AnimPath.c_str());
    if(m_pSprite == nullptr)
    {
        printf("IMG_Load error: %s\n%s\n", AnimPath, IMG_GetError());
        return false;
    }

    // create texture from loaded surface
    m_pTexture = SDL_CreateTextureFromSurface(m_pRenderer, m_pSprite);
    if(m_pTexture == nullptr)
    {
        printf("Error creating texture from surface: %s\n", SDL_GetError());
        return false;
    }

    // free surface from memory
    SDL_FreeSurface(m_pSprite);

    m_Rect.x = 0;
    m_Rect.y = 0;
    m_Rect.h = h;
    m_Rect.w = w;

    m_AnimFrameCount = count;
    m_AnimFrameHeight = h;
    m_AnimFrameWidth = w;

    return true;
}

void GAnimation::RenderAnim()
{
    if(m_bIsAlive)
    {
        SDL_Rect animRect;

        animRect.x = m_AnimFrameWidth * m_AnimPhase;
        animRect.y = 0;
        animRect.w = m_AnimFrameWidth;
        animRect.h = m_AnimFrameHeight;

        m_fAnimTimer += g_Timer->GetTime();
        if(m_fAnimTimer > 0.07f)
        {
            if(m_AnimPhase < m_AnimFrameCount)
                m_AnimPhase++;
            else
            {
                m_AnimPhase = 0;
                m_bIsAlive = false;
            }

            m_fAnimTimer = 0;
        }

        SDL_RenderCopy(m_pRenderer, m_pTexture, &animRect, &m_Rect);
    }
}

void GAnimation::Update()
{
    GAnimation::RenderAnim();
}
