#ifndef GPLAYERHPP
#define GPLAYERHPP

#include <list>

#include "GFramework.hpp"
#include "GSprite.hpp"
#include "GShot.hpp"
#include "GTimer.hpp"

class GPlayer{

    public:
        GPlayer();
        ~GPlayer();

        void Update();
        std::list<GShot*>* GetShotList() { return lShots; }

    private:
        void Render();
        void HandleMovement();
        void HandleShots();
        void HandleFlying();
        bool CheckBoundsLeft();
        bool CheckBoundsRight();
        bool CheckBoundsUpper();
        bool CheckBoundsLower();

        GSprite *m_pPlayerSprite;
        GSprite *m_pShotSprite;
        GShot *m_pShot;

        std::list<GShot*> *lShots;
        std::list<GShot*>::iterator lShotsIter;

        float m_fPlayerX;
        float m_fPlayerY;
        float m_fVelocity;
        float m_fAcceleration;
        float m_fSpeed;
        float m_fAngle;
        float m_fAngleSpeed;

        bool m_bShotLock;
        float m_fShotTimer;
};

#endif
