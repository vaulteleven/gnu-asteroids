#ifndef GSHOTHPP
#define GSHOTHPP

#include "GFramework.hpp"
#include "GSprite.hpp"

class GShot {

    public:
        // takes a pointer to a GSprite, so that one sprite can be used for 
        // all shots
        GShot(GSprite *sprite, float ShotX, float ShotY, float angle);
        ~GShot();

        void Update();
        void SetIsAlive(bool value) { m_bIsAlive = value; }
        bool GetIsAlive() { return m_bIsAlive; }
        SDL_Rect GetRect() { return m_ShotRect; }

    private:
        void Render();
        void HandleFlying();
        bool CheckBounds();

        SDL_Rect m_ShotRect;
        GSprite *m_pShotSprite;
        bool m_bIsAlive;
        float m_fShotX;
        float m_fShotY;
        float m_fVelocity;
        float m_fAngle;
};

#endif
