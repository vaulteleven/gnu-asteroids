#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "GGame.hpp"

int main(int argc, const char *argv[])
{
    puts("Game Started!");

    GGame *Game = new GGame(800, 800);
    puts("Game Created!");

    Game->Init();
    puts("Game Init!");

    Game->GameLoop();
    puts("Game End of GameLoop!");

    Game->Quit();
    puts("Game Quit!");
}
