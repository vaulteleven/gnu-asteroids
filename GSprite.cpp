#include "GSprite.hpp"

GSprite::GSprite()
{
    m_pRenderer = g_Framework->GetRenderer();
    m_pTexture = nullptr;
    m_bFullscreen = false;
    m_bIsAlive = false;
}

GSprite::~GSprite()
{
}

bool GSprite::Load(const std::string SpritePath)
{
    SDL_Surface *m_pSprite = IMG_Load(SpritePath.c_str());
    if(m_pSprite == nullptr)
    {
        printf("IMG_Load error: %s\n%s\n", SpritePath, IMG_GetError());
        return false;
    }

    // create texture from loaded surface
    m_pTexture = SDL_CreateTextureFromSurface(m_pRenderer, m_pSprite);
    if(m_pTexture == nullptr)
    {
        printf("Error creating texture from surface: %s\n", SDL_GetError());
        return false;
    }

    // free surface from memory
    SDL_FreeSurface(m_pSprite);

    m_Rect.x = 0;
    m_Rect.y = 0;
    m_Rect.h = m_pSprite->h;
    m_Rect.w = m_pSprite->w;

    return true;
}

bool GSprite::Load(const std::string SpritePath, bool fullscreen)
{
    SDL_Surface *m_pSprite = IMG_Load(SpritePath.c_str());
    if(m_pSprite == nullptr)
    {
        printf("IMG_Load error: %s\n%s\n", SpritePath, IMG_GetError());
        return false;
    }

    // create texture from loaded surface
    m_pTexture = SDL_CreateTextureFromSurface(m_pRenderer, m_pSprite);
    if(m_pTexture == nullptr)
    {
        printf("Error creating texture from surface: %s\n", SDL_GetError());
        return false;
    }

    // free surface from memory
    SDL_FreeSurface(m_pSprite);

    m_Rect.x = 0;
    m_Rect.y = 0;

    if(fullscreen)
    {
        m_Rect.h = g_Framework->GetWindowHeight();
        m_Rect.w = g_Framework->GetWindowWidth();
    }
    else
    {
        m_Rect.h = m_pSprite->h;
        m_Rect.w = m_pSprite->w;
    }

    return true;
}

void GSprite::SetPosition(int SpriteX, int SpriteY)
{
    // sets the position of the Sprite Rect
    m_Rect.x = SpriteX;
    m_Rect.y = SpriteY;
}

void GSprite::Render()
{
    SDL_RenderCopy(m_pRenderer, m_pTexture, nullptr, &m_Rect);
}

void GSprite::Render(double angle)
{
    SDL_RenderCopyEx(m_pRenderer, m_pTexture, nullptr, &m_Rect, 
            angle, nullptr, SDL_FLIP_NONE);
}
