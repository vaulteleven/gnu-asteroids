#include "GTimer.hpp"

GTimer::GTimer()
{
    m_fCurrentTime = 0.0f;
    m_fLastTime = SDL_GetTicks() / 1000.0f;
    m_fTimePassed = 0.0f;
}

GTimer::~GTimer()
{
    // TODO
}

void GTimer::UpdateTime()
{
    m_fCurrentTime = SDL_GetTicks() / 1000.0f;
    m_fTimePassed = m_fCurrentTime - m_fLastTime;
    m_fLastTime = m_fCurrentTime;
}

float GTimer::GetTime()
{
    return m_fTimePassed;
}
