#include "GFramework.hpp"

bool GFramework::Init(int WindowPosX, int WindowPosY, 
                      int WindowSizeW, int WindowSizeH)
{
    m_iWindowHeight = WindowSizeH;
    m_iWindowWidth = WindowSizeW;

    // initialize SDL subsystem 
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0)
    {
        printf("SDL_Init error: %s\n", SDL_GetError());

        Quit();

        return false;
    }

    // initialize SDL window
    m_pWindow = SDL_CreateWindow("stallman-asteroids", WindowPosX, WindowPosY,
            WindowSizeW, WindowSizeH, 0);
    if(m_pWindow == nullptr)
    {
        printf("SDL_CreateWindow error: %s\n", SDL_GetError());

        Quit();

        return false;
    }
    else
    {
        // create renderer on window
        m_pRenderer = SDL_CreateRenderer(m_pWindow, -1, 
                SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
        if(m_pRenderer == nullptr)
        {
            printf("Error creating the renderer err: %s\n", SDL_GetError());

            return false;
        }
        else 
        {
            // set the draw color for the renderer and its opacity
            SDL_SetRenderDrawColor(m_pRenderer, 0, 0, 0, 255);

            int imgFlags = IMG_INIT_PNG;
            if(!(IMG_Init(imgFlags) & imgFlags))
            {
                printf("Error Initializing IMG err: %s\n", IMG_GetError());
            }
        }

        /*
        m_pScreen = SDL_GetWindowSurface(m_pWindow);
        if(m_pScreen == nullptr)
        {
            printf("SDL_GetWindowSurface error: %s\n", SDL_GetError()); 

            Quit();

            return false;
        }
        */
    }

        // initialize the current keyboard state
    m_pKeyState = SDL_GetKeyboardState(nullptr);

    std::srand(SDL_GetTicks());

    return true;
}

int GFramework::GetRandomNumber()
{
    m_iRandomNumber = rand() % m_iWindowWidth;

    return m_iRandomNumber;
}

int GFramework::GetRandomNumber(int min, int max)
{
    m_iRandomNumber = rand() % max + min;

    return m_iRandomNumber;
}

void GFramework::Quit()
{
    if(m_pWindow)
        SDL_DestroyWindow(m_pWindow);

    IMG_Quit();
    SDL_Quit();
}

void GFramework::Update()
{
    // update timer 
    g_Timer->UpdateTime();

    SDL_PumpEvents();
}

void GFramework::Clear()
{
    // clear window buffer
    //SDL_FillRect(m_pScreen, nullptr, 
    //        SDL_MapRGB(m_pScreen->format, 0, 0, 0));
    SDL_RenderClear(m_pRenderer);
}

void GFramework::UpdateScreen()
{
    // updated the window surface with new drawn stuff
    //SDL_UpdateWindowSurface(m_pWindow);
    SDL_RenderPresent(m_pRenderer);
}

bool GFramework::KeyDown(int Key_ID)
{
    // return true when corresponding key with Key_ID is pressed
    if(m_pKeyState[Key_ID])
    {
        return true;
    }
    else
    {
        return false;
    }
}
