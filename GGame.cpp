#include "GGame.hpp"

GGame::GGame(int screenwidth, int screenheight)
{
    m_ScreenWidth = screenwidth;
    m_ScreenHeight = screenheight;

    m_MaxAsteroids = 5;

    m_bEnableAsteroids = true;
}

GGame::~GGame()
{
    delete(m_pBackgroundSprite);
    m_pBackgroundSprite = nullptr;

    delete(m_pPlayer);
    m_pPlayer = nullptr;

    delete(m_pAsteroidSprite);
    m_pAsteroidSprite = nullptr;
}

bool GGame::Init()
{
    if(!g_Framework->Init(0, 0, m_ScreenWidth, m_ScreenHeight))
        return false;

    g_Framework->Clear();
    g_Framework->Update();

    m_pBackgroundSprite = new GSprite();
    m_pBackgroundSprite->Load("assets/images/background_space.jpg", true);

    m_pAsteroidAnim = new GAnimation();
    m_pAsteroidAnim->LoadAnim("assets/images/asteroid_anim.png", 59, 59, 4);

    m_pAsteroidSprite = new GSprite();
    m_pAsteroidSprite->Load("assets/images/asteroid.png");
    
    m_pPlayer = new GPlayer();
    m_lShotList = m_pPlayer->GetShotList();

    m_bGameRunning = true;

    return true;
}

void GGame::GameLoop()
{
    // while the Escape key is not pressed, keep the game running
    while(m_bGameRunning)
    {
        // update timer and keyboard event array 
        g_Framework->Clear();
        g_Framework->Update();

        // render background png graphic
        m_pBackgroundSprite->Render();

        // check if escape key was pressed
        if(g_Framework->KeyDown(SDL_SCANCODE_ESCAPE))
            m_bGameRunning = false;

        if(m_bEnableAsteroids)
        {
            GGame::HandleAsteroids();
            GGame::HandleCollisions();
            GGame::HandleAnimations();
        }

        m_pPlayer->Update();

        // update sdl window surface to show changes
        g_Framework->UpdateScreen();
    }
}

void GGame::HandleAnimations()
{
    // if asteroid has been hit with shot, render animation,
    // delete it from the asteroid list, create new asteroid and push
    // it to the list.
    for(m_lAsteroidAnimIter = m_lAsteroidAnimation.begin();
            m_lAsteroidAnimIter != m_lAsteroidAnimation.end();
            m_lAsteroidAnimIter++)
    {
        if((*m_lAsteroidAnimIter)->GetIsAlive())
        {
            (*m_lAsteroidAnimIter)->Update();
        }
        else
        {
            delete(*m_lAsteroidAnimIter);
            (*m_lAsteroidAnimIter) = nullptr;

            m_lAsteroidAnimIter = 
                m_lAsteroidAnimation.erase(m_lAsteroidAnimIter);
        }
    }
}

void GGame::HandleAsteroids()
{
    // check if list is empty, if it is create maximum amount of asteroids and
    // add it to the list
    if(m_AsteroidList.size() == 0)
    {
        int i;
        for(i = 0; i <= m_MaxAsteroids; i++)
        {
            GAsteroid *asteroid = new GAsteroid(m_pAsteroidSprite);
            asteroid->SetIsAlive(true);
            m_AsteroidList.push_back(asteroid);
        }
    }

    // iterate through asteroid list and update each one
    for(m_AsteroidIter = m_AsteroidList.begin(); 
            m_AsteroidIter != m_AsteroidList.end();
            m_AsteroidIter++)
    {
        // if asteroid is still alive update it
        if((*m_AsteroidIter)->GetIsAlive())
        {
            (*m_AsteroidIter)->Update(); 
        }
        else
        {
            delete(*m_AsteroidIter);
            (*m_AsteroidIter) = nullptr;

            GAsteroid *asteroid = new GAsteroid(m_pAsteroidSprite);
            m_AsteroidIter = m_AsteroidList.erase(m_AsteroidIter);
            m_AsteroidList.push_back(asteroid);
        }
    }
}

void GGame::HandleCollisions()
{
    SDL_Rect rShot;
    SDL_Rect rAsteroid;

    for(m_AsteroidIter = m_AsteroidList.begin();
            m_AsteroidIter != m_AsteroidList.end();
            m_AsteroidIter++)
    {
        if(m_lShotList->size() > 0)
        {
            for(m_lShotIter = m_lShotList->begin();
                    m_lShotIter != m_lShotList->end();
                    m_lShotIter++)
            {
                rShot = (*m_lShotIter)->GetRect();
                rAsteroid = (*m_AsteroidIter)->GetRect();

                // handle collision between shots and asteroids
                if(rShot.y < rAsteroid.y + rAsteroid.h &&
                        rShot.y + rShot.h > rAsteroid.y &&
                        rShot.x + rShot.w > rAsteroid.x &&
                        rShot.x < rAsteroid.x + rAsteroid.w)
                {
                    // this is executed once per asteroid hit!

                    // create Asteroid Animation GAnimation and push it to 
                    // the animation list;
                    GAnimation *asteroidAnim = new GAnimation();
                    asteroidAnim->LoadAnim(
                            m_pAsteroidAnim->GetTexture(), 59, 59, 4);
                    asteroidAnim->SetIsAlive(true);
                    asteroidAnim->SetPosition(rAsteroid.x, rAsteroid.y);
                    m_lAsteroidAnimation.push_back(asteroidAnim);
                    
                    (*m_AsteroidIter)->SetIsAlive(false);

                    (*m_lShotIter)->SetIsAlive(false);
                }
            }
        }
    }
}

void GGame::Render()
{
    
}

void GGame::Quit()
{
    if(m_pAsteroidAnim->GetTexture())
        SDL_DestroyTexture(m_pAsteroidAnim->GetTexture());

    delete(m_pPlayer);
    m_pPlayer = nullptr;

    SDL_DestroyTexture(m_pBackgroundSprite->GetTexture());
    delete(m_pBackgroundSprite);
    m_pBackgroundSprite = nullptr;

    SDL_DestroyTexture(m_pAsteroidSprite->GetTexture());
    delete(m_pAsteroidSprite);
    m_pAsteroidSprite = nullptr;

    SDL_DestroyTexture(m_pAsteroidAnim->GetTexture());
    delete(m_pAsteroidAnim);
    m_pAsteroidAnim = nullptr;

    delete(m_lShotList);
    m_lShotList = nullptr;

    g_Framework->Quit();
}
