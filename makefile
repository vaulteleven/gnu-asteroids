OBJS = main.o GTimer.o GFramework.o GSprite.o GPlayer.o GShot.o GAsteroid.o GGame.o GAnimation.o

CC = g++

COMPILER_FLAGS = -w

LINKER_FLAGS = -lSDL2 -lSDL2_image

OBJ_NAME = gnu-asteroids

clean:
	\rm *.o 

all : $(OBJS)
	$(CC) $(OBJS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OBJ_NAME)
