#include "GAsteroid.hpp"

GAsteroid::GAsteroid(GSprite *sprite)
{
    m_pAsteroidSprite = sprite;

    // create a new animation and input height, width and number of animation
    // frames
    //
    /*
    m_pAsteroidSpriteAnim = new GSprite();
    m_pAsteroidSpriteAnim->LoadAnim("assets/images/asteroid_anim.png", 47, 60, 4);
    */

    GAsteroid::HandleStartingPosition();

    m_bIsAlive = true;

    // velocity is random from minimum 200 to 300 max speed
    m_fVelocity = g_Framework->GetRandomNumber(100, 200);
    // the faster the asteroid is going, the faster it rotates
    m_dAngleSpeed = 0.05f + (m_fVelocity) / 1000;
    m_dAngle = 0.0f;
}

GAsteroid::~GAsteroid()
{
}

void GAsteroid::Render()
{
    // set the new position of the asteroid and rotate it with m_dAngle
    m_pAsteroidSprite->SetPosition(m_fAsteroidX, m_fAsteroidY);
    m_pAsteroidSprite->Render(m_dAngle);
}

void GAsteroid::HandleFlying()
{
    m_fAsteroidY += m_fVelocity * g_Timer->GetTime();
    m_AsteroidRect.y = m_fAsteroidY;

    // increment the angle constantly to let the asteroids rotate
    if(m_dAngle >= 360.0)
        m_dAngle = 0.0;

    m_dAngle += m_dAngleSpeed;
}

void GAsteroid::HandleStartingPosition()
{
    int randomNumberX = g_Framework->GetRandomNumber(); 

    if(randomNumberX > g_Framework->GetWindowWidth() - m_pAsteroidSprite->GetRect().w)
        randomNumberX -= m_pAsteroidSprite->GetRect().w;

    m_fAsteroidX = randomNumberX;

    int randomNumberY = -1 * 
        (g_Framework->GetRandomNumber(0, 400) + m_pAsteroidSprite->GetRect().h); 

    m_fAsteroidY = randomNumberY;

    m_AsteroidRect.x = m_fAsteroidX;
    m_AsteroidRect.y = m_fAsteroidY;
    m_AsteroidRect.w = m_pAsteroidSprite->GetRect().w;
    m_AsteroidRect.h = m_pAsteroidSprite->GetRect().h;
}

bool GAsteroid::CheckBounds()
{
    // when the asteroid leaves the screen it is not alive anymore
    // this function needs a rework for when the asteroid is flying sideways 
    // aswell TODO
    if(m_fAsteroidY > g_Framework->GetWindowHeight())
    {
        m_bIsAlive = false;
        return false;
    }

    return true;
}

void GAsteroid::Update()
{
    if(m_bIsAlive)
    {
        GAsteroid::Render();
        GAsteroid::HandleFlying();
        GAsteroid::CheckBounds();
    }
}
