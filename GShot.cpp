#include "GShot.hpp"
#include <math.h>

GShot::GShot(GSprite *sprite, float ShotX, float ShotY, float angle)
{
    // create new shot and load the sprite for the shot
    m_pShotSprite = sprite;

    m_fShotX = ShotX;
    m_fShotY = ShotY;

    m_ShotRect.x = ShotX;
    m_ShotRect.y = ShotY;
    m_ShotRect.w = sprite->GetRect().w;
    m_ShotRect.h = sprite->GetRect().h;

    m_pShotSprite->SetPosition(m_fShotX, m_fShotY);

    m_fVelocity = 800.0f;
    m_bIsAlive = true;
    m_fAngle = angle;
}

GShot::~GShot()
{

}

void GShot::Render()
{
    // if shot is still alive render it 
    if(m_bIsAlive)
    {
        m_pShotSprite->SetPosition(m_fShotX, m_fShotY);
        m_pShotSprite->Render(m_fAngle); 
    }

}

void GShot::HandleFlying()
{
    m_fShotX += m_fVelocity * g_Timer->GetTime() * sin(m_fAngle*M_PI/180);
    m_fShotY -= m_fVelocity * g_Timer->GetTime() * cos(m_fAngle*M_PI/180);

    m_ShotRect.x = m_fShotX;
    m_ShotRect.y = m_fShotY;
}

// checks wether the shot is out of bounds yet, if yes set isAlive to false
bool GShot::CheckBounds()
{
    if(m_fShotY <= (0 - m_pShotSprite->GetRect().h))
    {
        SetIsAlive(false);
        return false;
    }
    if(m_fShotX <= (0 - m_pShotSprite->GetRect().w || 
       m_fShotX >= g_Framework->GetWindowWidth()))
    {
        SetIsAlive(false);
        return false;
    }

    return true;
}

// Handles rendering and updating the Shots position
void GShot::Update()
{
    GShot::Render();
    GShot::HandleFlying();
    GShot::CheckBounds();
}
