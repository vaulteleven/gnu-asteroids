#ifndef GASTEROIDHPP
#define GASTEROIDHPP

#include "GFramework.hpp"
#include "GSprite.hpp"

class GAsteroid{

    public:
        // takes a pointer to a GSprite, so one sprite object can be reused
        // for all asteroids
        GAsteroid(GSprite *sprite);
        ~GAsteroid();

        void Update();
        void SetIsAlive(bool value) { m_bIsAlive = value; }
        void SetVelocity(float velocity) { m_fVelocity = velocity; }
        bool GetIsAlive() { return m_bIsAlive; }
        GSprite *GetAnim() { return m_pAsteroidSpriteAnim; }
        void HandleStartingPosition();
        SDL_Rect GetRect() { return m_AsteroidRect; }

    private:
        void Render();
        void HandleFlying();
        bool CheckBounds();

        GSprite *m_pAsteroidSprite;
        GSprite *m_pAsteroidSpriteAnim;
        SDL_Rect m_AsteroidRect;
        bool m_bIsAlive;
        float m_fAsteroidX;
        float m_fAsteroidY;
        float m_fVelocity;
        double m_dAngle;
        double m_dAngleSpeed;
};

#endif
