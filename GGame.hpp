#ifndef GGAMEHPP
#define GGAMEHPP

#include <list>

#include "GFramework.hpp"
#include "GPlayer.hpp"
#include "GShot.hpp"
#include "GSprite.hpp"
#include "GAsteroid.hpp"
#include "GAnimation.hpp"

class GGame {
    
    public:
        GGame(int screenwidth, int screenheight);
        ~GGame();

        bool Init();
        void GameLoop();
        void Quit();
        void Render();

    private:
        void HandleAsteroids();
        void HandleCollisions();
        void HandleAnimations();

        GPlayer *m_pPlayer;
        GSprite *m_pBackgroundSprite; 
        GSprite *m_pAsteroidSprite; 
        GAnimation *m_pAsteroidAnim;

        bool m_bGameRunning;
        int m_MaxAsteroids;
        int m_ScreenWidth;
        int m_ScreenHeight;

        // gamestate variables
        bool m_bEnableAsteroids;

        std::list<GAsteroid*> m_AsteroidList;
        std::list<GAsteroid*>::iterator m_AsteroidIter;
        std::list<GShot*> *m_lShotList;
        std::list<GShot*>::iterator m_lShotIter;
        std::list<GAnimation*> m_lAsteroidAnimation;
        std::list<GAnimation*>::iterator m_lAsteroidAnimIter;
};

#endif
