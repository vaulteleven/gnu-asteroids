#ifndef GFRAMEWORK_HPP
#define GFRAMEWORK_HPP

#include <SDL2/SDL_image.h>
#include <iostream>
#include <random>
#include <math.h>

#include "TSingleton.hpp"
#include "GTimer.hpp"

#define g_Framework GFramework::Get()
class GFramework : public TSingleton<GFramework>
{
    public:
        bool Init(int WindowPosX, int WindowPosY, 
                  int WindowSizeW, int WindowSizeH);
        void Quit();
        void Update();
        void Clear();
        void UpdateScreen();
        bool KeyDown(int Key_ID);
        int GetRandomNumber();
        int GetRandomNumber(int min, int max);

        SDL_Window *GetWindow() { return m_pWindow; }
        SDL_Renderer *GetRenderer() { return m_pRenderer; }
        SDL_Surface *GetScreen() { return m_pScreen; }

        int GetWindowHeight() { return m_iWindowHeight; }
        int GetWindowWidth() { return m_iWindowWidth; }

    private:
        SDL_Window *m_pWindow;
        SDL_Renderer *m_pRenderer;
        SDL_Surface *m_pScreen;
        const Uint8 *m_pKeyState;

        int m_iWindowHeight;
        int m_iWindowWidth;

        // random number stuff
        int m_iRandomNumber;
};

#endif
